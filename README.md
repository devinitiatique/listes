

# TD 11 : Listes chaînées

On donne les classes `Liste`, `Maillon` et ` MainListe` et
les compléter avec les algorithmes ci-dessous. La classe `Liste`
implante les listes chaînées d'éléments de type entier.

# Algorithmes de base sur les listes

Ecrire les fonctions suivantes. Pour chacune d'elles, vérifier bien que
vous traitez **tous les cas de figure** : liste vide, liste à un seul
élément, etc.


**Question 1**. Longueur d'une liste 

`public int longueur()` : Retourne le nombre d'éléments de la liste 'this'.  
   
Notez que cette méthode parcourt tous les maillons de la liste. Pour
des raisons d'efficacité (et de pédagogie), sauf mention contraire, il
sera donc **interdit** de faire appel à la fonction `longueur` pour répondre
aux questions suivantes.

**Question 2**. Somme des éléments 

`public int somme()` : Détermine la somme des éléments de la liste 'this'. 

**Question 3**. Maximum des éléments 

`public int maximum()`: Détermine la valeur maximum des éléments d'une liste.

Prérequis : la liste est non vide.

**Question 4**. Nombre d'occurrences de `n`

`public int nbOcc()`: Détermine le nombre d'occurrences d'un entier `n` dans une liste.

**Question 5**. Clones 

`public boolean estClone (Liste l)` : Détermine si deux listes (this et l) sont clones l'une de l'autre, c'est-à-dire ont les mêmes valeurs rangées dans le même ordre.

**Question 6**. Longueur supérieure à `k`?

  Prérequis  : `k` est un entier positif ou nul
  
`public boolean estSupK (int k) ` : Retourne vrai ssi la longueur de la liste 'this' est supérieure ou égale à k, faux sinon.
  
Contrainte : pensez à un parcours partiel (ex : si k=1)
   
**Question 7**. Dernier élément 

 Prérequis : 'this' est non vide.  
 `public int dernierElt ()` : Retourne le dernier maillon de la liste 'this'.
  

**Question 8**. Ajout en fin de liste 

   Prérequis : Aucun !  
   `public void ajoutFin(int n)`:  Ajoute un élément de valeur `n` comme dernier élément de la liste 'this' 
  
Vous pouvez éventuellement utiliser la méthode `dernierMaillon` pour
écrire cette méthode.

Le premier bonus vous permettra d'écrire une version plus efficace de
`ajoutFin`.

**Question 9**. Ajout en fin de liste si absent

`public void ajoutFinSiAbsent (int n)`:  Ajoute un élément de valeur `n` comme dernier élément de la liste 'this', seulement si la liste ne possède pas déjà un élément valant `n`. 

Contrainte : la méthode parcourt la liste au maximum une fois.
    
**Question 10**. Extraction des éléments impairs 

 `public Liste extractionImpairs`  :  Retourne une nouvelle liste contenant les éléments de valeur impaire de 'this'.
  Ecrire une méthode pour dans chacun des 2 cas suivants (Version 1 et version 2 ) :
  
* l'ordre des éléments de la liste retournée n'a pas d'importance,
* l'ordre doit être le même que dans 'this'. 
     
    
**Question 11**. Troncation après le `k ème` élément

`public void tronquerK (int k)`: Retourne la liste `this` tronquée après son `k ème` élément (si un tel élément existe)

**Question 12**. Suppression d'un élément 

Pré-requis : aucun 

Action   : supprime de la liste 'this' la première occurrence d'un entier `n`.

Résultat : retourne vrai si l'élément` n` a été trouvé, faux sinon.
   
`public boolean supprOcc (int n)

**Question 13**. Liste à l'envers 

Pré-requis : aucun 

`public Liste inverse() ` : retourne une nouvelle liste contenant les éléments de 'this' dans l'ordre inverse. 
   

# Exercices plus difficiles


**Question 14**. Liste à l'envers, récursive et en place (bonus difficile) 

`public void inverseRec() ` : inverse les éléments de 'this' en faisant appel à une fonction récursive "private Maillon inverseRec (Maillon m)". 

Pré-requis : aucun 


`private Maillon inverseRec (Maillon m)`  : sans créer de nouveau maillon, inverse la liste 'this' à partir du maillon m et renvoie la ref du nouveau maillon de tête.


Pour les exercices suivants, il est fortement recommandé de bien
*tester* vos algorithmes. Vous trouverez en commentaires de
` MainListe.java` des exemples compliqués pour `suppToutesOcc` et
`sousListe`.


**Question 15**. Suppression de toutes les occurrences d'une valeur `n`

 `public void suppToutesOcc(int n)` :  supprime de la liste 'this' toutes les occurrences d'un entier `n`. 
Contrainte : La version demandée ne parcourt la liste qu'une seule fois ! 
(Une version simple à écrire mais coûteuse fait appel à supprOcc.)                
    
**Question 16**. Sous-liste 

`public boolean sousListe (Liste l)` retourner vrai ssi 'this' est une sous-liste (éléments consécutifs) de l.
 
 Exemples : cf. `mainListe.java`
 
 Stratégie possible : parcourt l et fait appel à chaque itération à une fonction estPrefixe
  

`private boolean estPrefixe (Maillon m)` : retourner vrai ssi la suite des éléments de 'this' est un préfixe de la liste l à partir du maillon m, c’est-à-dire est égale à la suite des premiers  éléments de l commençant au maillon m.

Exemple : (3,3,4) est un préfixe de la liste (1,6,3,3,4,2) à partir du 3ème maillon.
   

# Bonus : des listes plus riches


**Question 17**. Créer le plus vite possible une classe `Liste2`
(testée par ` MainListe2`) qui a des spécifications très proches de
`Liste`, mais qui contient deux autres attributs pour améliorer les
performances en temps de calcul CPU :*

-   un attribut `longueur` de type entier qui stocke la longueur de la liste `this` ;

-   un attribut `dernier` qui est une référence sur le dernier maillon
    de la liste `this`. Cet attribut permet d'améliorer les performances
    des méthodes travaillant en fin de liste.